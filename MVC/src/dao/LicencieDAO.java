package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Club;
import classes.Licencie;

public class LicencieDAO extends DAO<Licencie> {

	@Override
	public List<Licencie> recupAll() {
		// d�finition de la liste qui sera retourn�e en fin de m�thode
		List<Licencie> listeLicencies = new ArrayList<Licencie>(); 
		
		// d�claration de l'objet qui servira pour la requ�te SQL
		try {
			Statement requete = this.connect.createStatement();

			// d�finition de l'objet qui r�cup�re le r�sultat de l'ex�cution de la requ�te
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".licencie");

			// tant qu'il y a une ligne "r�sultat" � lire
			while (curseur.next()){
				// objet pour la r�cup d'une ligne de la table Licencie
				Licencie unLicencie = new Licencie();
				
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("dateNaiss"));
				
				listeLicencies.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeLicencies; 
	}
	@Override
	public List<Licencie> recupAllByClub(Club c) {
		// d�finition de la liste qui sera retourn�e en fin de m�thode
		List<Licencie> listeLicencies = new ArrayList<Licencie>(); 
		
		// d�claration de l'objet qui servira pour la requ�te SQL
		try {
			Statement requete = this.connect.createStatement();

			// d�finition de l'objet qui r�cup�re le r�sultat de l'ex�cution de la requ�te
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".licencie where club = '"+c.getCode()+"'");

			// tant qu'il y a une ligne "r�sultat" � lire
			while (curseur.next()){
				// objet pour la r�cup d'une ligne de la table Licencie
				Licencie unLicencie = new Licencie();
				
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("dateNaiss"));
				unLicencie.setClub(new ClubDAO().read(curseur.getString("club")));
				
				listeLicencies.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeLicencies; 
	}
	
	// Insertion d'un objet Licencie dans la table Licencie (1 ligne)
	@Override
	public void create(Licencie obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"mvc\".licencie VALUES(?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setString(3, obj.getPrenom());
				prepare.setString(4, obj.getDateNaiss());
					
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un Licencie par rapport � son code	
	@Override
	public Licencie read(String code) {
	
		Licencie leLicencie = new Licencie();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"mvc\".licencie WHERE code = '" + code +"'");
	          
	          if(result.first())
	           		leLicencie = new Licencie(code, result.getString("nom"),result.getString("prenom"),result.getString("datenaiss"), new ClubDAO().read(result.getString("club")));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leLicencie;
		
	}
		
	
	// Mise � jour d'un Licencie
	@Override
	public void update(Licencie obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"mvc\".licencie SET nom = '" + obj.getNom() + "'"+
	                    	      " WHERE code = '" + obj.getCode()+"'" );
				
			  obj = this.read(obj.getCode());
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
		
		//return obj;
	}


	// Suppression d'un Licencie
	@Override
	public void delete(Licencie obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"mvc\".licencie WHERE code = '" + obj.getCode()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}

	
}
