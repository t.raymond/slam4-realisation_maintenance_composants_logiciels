package dao;

import java.sql.Connection;
import java.util.List;

import classes.Club;


public abstract class DAO<T> {
	
	
	public static Connection connect= ConnexionPostgreSql.getInstance();
	
	
	public abstract void create(T obj);
	
	
	
	public abstract T read(String CP);
	

	public abstract void delete(T obj);
	

	public abstract void update(T obj);
	

	public abstract List<T> recupAll();

	public abstract List<T> recupAllByClub(Club c);

}
