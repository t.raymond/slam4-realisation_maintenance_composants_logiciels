package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Club;

public class ClubDAO extends DAO<Club> {

	@Override
	public List<Club> recupAll() {
		// d�finition de la liste qui sera retourn�e en fin de m�thode
		List<Club> listeClubs = new ArrayList<Club>(); 
		
		// d�claration de l'objet qui servira pour la requ�te SQL
		try {
			Statement requete = this.connect.createStatement();

			// d�finition de l'objet qui r�cup�re le r�sultat de l'ex�cution de la requ�te
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".club");

			// tant qu'il y a une ligne "r�sultat" � lire
			while (curseur.next()){
				// objet pour la r�cup d'une ligne de la table Club
				Club unClub = new Club();
				
				unClub.setCode(curseur.getString("code"));
				unClub.setNom(curseur.getString("nom"));
				unClub.setNom_president(curseur.getString("nom_president"));
				unClub.setNom_entraineur(curseur.getString("nom_entraineur"));
				
				listeClubs.add(unClub);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeClubs; 
	}
	
	
	// Insertion d'un objet Club dans la table Club (1 ligne)
	@Override
	public void create(Club obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"mvc\".club VALUES(?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setString(3, obj.getNom_president());
				prepare.setString(4, obj.getNom_entraineur());
					
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un club par rapport � son code	
	@Override
	public Club read(String code) {
	
		Club leClub = new Club();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"mvc\".club WHERE code = '" + code +"'");
	          
	          if(result.first())
	           		leClub = new Club(code, result.getString("nom"),result.getString("nom_president"),result.getString("nom_entraineur"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leClub;
		
	}
		
	
	// Mise � jour d'un Club
	@Override
	public void update(Club obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"mvc\".club SET nom = '" + obj.getNom() + "'"+
	                    	      " WHERE code = '" + obj.getCode()+"'" );
				
			  obj = this.read(obj.getCode());
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
		
		//return obj;
	}


	// Suppression d'un Club
	@Override
	public void delete(Club obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"mvc\".club WHERE code = '" + obj.getCode()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}




	@Override
	public List<Club> recupAllByClub(Club c) {
		// TODO Auto-generated method stub
		return null;
	}
}
