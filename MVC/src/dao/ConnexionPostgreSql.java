package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {

	public static Connection connect = null;
	private static String url = "jdbc:postgresql://postgresql.bts-malraux72.net:5432/t.raymond", user = "t.raymond", passwd ="P@ssword";
	
	public static Connection getInstance(){
		if(connect == null){
		try {
		connect = DriverManager.getConnection(url, user, passwd);
		} catch (SQLException e) {
		e.printStackTrace();
		}
		}
		return connect;
		}
}
