package controleur;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import classes.Club;
import classes.Licencie;
import dao.ClubDAO;
import dao.DAO;
import dao.LicencieDAO;
import vues.MaVue;

public class ControleurPrincipal implements ActionListener {
// définition de l’objet instance de MaVue (vue principale)
private MaVue vue;
// déclaration des objets Modele qui permettront d’obtenir ou de transmettre les données :
private DAO<Club> gestionClub;
private DAO<Licencie> gestionLicencie;
public List<Club> lesClubs = new ArrayList<Club>();
// déclarations des éventuelles propriétés utiles au contrôleur
//... /...
// Constructeur
public ControleurPrincipal()
{
this.gestionClub=new ClubDAO();
this.gestionLicencie= new LicencieDAO();
this.lesClubs = gestionClub.recupAll();

// + initialisations éventuelles autres propriétés définies dans cette classe
}
@Override
public void actionPerformed(ActionEvent e)
// Méthode qu'il faut implémenter
{
	
	
	
if (e.getActionCommand().equals("choixC"))
{
	//Club c88 = new Club("888","Le Mans FC","Ren� Sance","Paul Emploi ");
	//lesClubs.add(c88);
	//vue.remplirCombo(lesClubs);
	if (vue.comboBox.getSelectedIndex()!=0)
	{
		int index = vue.comboBox.getSelectedIndex()-1;
		System.out.println(lesClubs.get(index).toString());
		vue.affichageInfoClub(lesClubs.get(index).toString());
		vue.affichageLicencie(gestionLicencie.recupAllByClub(lesClubs.get(index)));
		
		//a faire
		
		//vue.affichageLicencies
		//lesClubs.add(new Club("0888","Le Mans FC","Ren� Sance","Paul Emploi "));
		/*for (Club c : lesClubs) {
			System.out.println(c);
			
		}*/
		
		
	}
}
	
	
// quand on clique sur Quitter
if (e.getActionCommand().equals("quit"))
{
	System.exit(0);
System.out.println("jj");
Quitter(); // déconnection bdd et arrêt de l’application
}
// si on clique sur le bouton enregistrer (un nouveau licencié)
else if (e.getActionCommand().equals("ajouter"))
{
	
	
	vue.remplirCombo(gestionClub.recupAll());
	
	//Club c88 = new Club("888","Le Mans FC","Ren� Sance","Paul Emploi ");
	//lesClubs.add(new Club("888","Le Mans FC","Ren� Sance","Paul Emploi "));
	//lesClubs.add(new Club("0888","Le Mans FC","Ren� Sance","Paul Emploi "));
	//vue.remplirCombo();
// insertion d’un nouveau licencié dans la bdd en récupérant les données de la vue
// retour à la vue avec mise à jour des licenciés du club dans la zone prévue
 //vue.majPanel('A');
}
else if (e.getActionCommand().equals("enregistrer"))
{
// insertion d’un nouveau licencié dans la bdd en récupérant les données de la vue
// retour à la vue avec mise à jour des licenciés du club dans la zone prévue
 vue.majPanel('A');
}
}
private void Quitter() {
	// TODO Auto-generated method stub
	System.exit(0);
	
}
public void setVue(MaVue view) {
this.vue = view;
vue.remplirCombo(gestionClub.recupAll());
}
}
// si un autre évènement est à l’origine de l’appel au contrôleur
//else if (e.getActionCommand().equals( "xxxxx" ) {