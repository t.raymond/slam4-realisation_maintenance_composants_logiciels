package classes;
/**
 * Classe Licencie
 * @author Thomas Rmd
 *
 */
public class Licencie {
	
	private String code;
	private String nom;
	private String prenom;
	private String dateNaiss;
	private Club leClub;
	
	/**
	 * Permet d'obtenir le code de licensie
	 * @return code : String
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Permet la modification du code de Licencie
	 * @param code - String
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * Permet d'obtenir le non de licencie
	 * @return nom - String
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Permet la modification du nom d licencie
	 * @param nom - String
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Permet d'obtenir le prenom de licencie
	 * @return prenom - String
	 */
	public String getPrenom() {
		return prenom;
	}
	
	/**
	 * Permet la modification du prenom du licencie
	 * @param prenom - String
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * Permet d'obtenir la date de naissance du licencie
	 * @return dateNaiss - String
	 */
	public String getDateNaiss() {
		return dateNaiss;
	}
	
	/**
	 * Permet la modification de la date naissance du licenscier
	 * @param dateNaiss - String
	 */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	
	/**
	 * Constructeur par d�faut - permet l'initialisation de toutes les propri�t�s
	 */
	public Licencie() {
		code = null;
		nom = null;
		prenom = null;
		dateNaiss = null;
		leClub=new Club();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructeur avec param�tres - permet l'initialisation de toutes les propri�t�s
	 * @param code - String
	 * @param nom - String
	 * @param prenom - String
	 * @param dateNaiss - String
	 */
	public Licencie(String code, String nom, String prenom, String dateNaiss,Club leClub) {
		
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.leClub = leClub;
	}
	
	/**
	 * Renvoir une chaine de caract�re avec valeur des propri�t�s
	 */
	@Override
	public String toString() {
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss + "]";
	}

	public void setClub(Club c) {
		// TODO Auto-generated method stub
		this.leClub = c;
	}
	
	
	

}
