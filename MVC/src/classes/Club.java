package classes;
/**
 * Classe Club
 * @author Thomas Rmd
 *
 */
public class Club {

	private String code;
	private String nom;
	private String nom_president;
	private String nom_entraineur;
	/**
	 * Permet d'obtenir le code du club
	 * @return code - String
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * permet la modification du code club
	 * @param code - String
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Permet d'obtenir le nom du club
	 * @return nom - String
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * permet la modification du nom du club
	 * @param nom - String
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Permet d'obtenir le nom du pr�sident du club
	 * @return nom_president - String
	 */
	public String getNom_president() {
		return nom_president;
	}
	
	/**
	 * permet la modification du nom du presiendt du club
	 * @param nom_president - String
	 */
	public void setNom_president(String nom_president) {
		this.nom_president = nom_president;
	}
	/**
	 * Permet d'obtenir le nom de l'entraineur du club
	 * @return nom_entraineur - String
	 */
	public String getNom_entraineur() {
		return nom_entraineur;
	}
	/**
	 * permet la modification du nom de l'entraineur du club
	 * @param nom_entraineur - String
	 */
	public void setNom_entraineur(String nom_entraineur) {
		this.nom_entraineur = nom_entraineur;
	}
	
	/**
	 * Constructeur par d�faut - permet d'initialiser toutes les propri�t�s
	 */
	public Club() {
		
		code = null;
		nom = null;
		nom_president = null;
		nom_entraineur = null;

	}
	/**
	 * Constructeur avec param�tres - permet d'initialiser toutes les propri�t�s
	 * @param code - String
	 * @param nom - String
	 * @param nom_president - String
	 * @param nom_entraineur - String
	 */
	public Club(String code, String nom, String nom_president, String nom_entraineur) {
		
		this.code = code;
		this.nom = nom;
		this.nom_president = nom_president;
		this.nom_entraineur = nom_entraineur;
	}
	
	/**
	 * Renvoir une chaine de caract�re avec valeur des propri�t�s
	 */
	@Override
	public String toString() {
		return "Club [code=" + code + ", nom=" + nom + ", nom_president=" + "\n" + nom_president + ", nom_entraineur="
				+ nom_entraineur + "]";
	}
	
	
}
