package tests;

import dao.ClubDAO;
import dao.LicencieDAO;
import dao.DAO;
import classes.Licencie;
import classes.Club;

public class testDAO {

	public static void main() {
		DAO<Licencie> licencie = new LicencieDAO();
		DAO<Club> club = new ClubDAO();
		// test de la recherche d'un licenciÃ© en fonction de son code
		System.out.println(licencie.read("TAGe").toString());
		// test de la recherche d'un club en fonction de son code
		Club unCLub = club.read("C1");
		Licencie unLicencie = new Licencie("CACl", "CALVIN","ClÃ©mence","17/05/1990", unCLub);
		// test de l'insertion d'un nouveau licenciÃ©
		licencie.create(unLicencie);
		
		//test de l'insertion d'un nouveau club
		club.create(unCLub);
		
		//test de la modification d'un club
		club.update(unCLub);
		
		for(Club c : club.recupAll()) {
		
		c.getNom();
		}
		//test de la suppression d'un club
		club.delete(unCLub);
		
		//test de la modification d'un licencie
		licencie.update(unLicencie);
		
		for(Licencie l : licencie.recupAll()) {
			
			l.getNom();
			}
		//test de la suppression d'un licencie
		licencie.delete(unLicencie);
}
}

