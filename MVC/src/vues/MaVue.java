package vues;
import classes.Club;
import classes.Licencie;

import java.awt.BorderLayout;
import classes.Club;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import classes.Club;
import controleur.ControleurPrincipal;

import java.util.ArrayList;
import java.util.List;
import controleur.ControleurPrincipal;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MaVue extends JFrame {

	private JPanel contentPane;
	protected ControleurPrincipal controleur;
	public JComboBox comboBox;
	public JLabel lblAffichageClub;
	public JTextArea AffichageListLicencie;
    

	/**
	 * Create the frame.
	 * @param controleur 
	 */
	public MaVue(ControleurPrincipal controleur) {
		this.controleur=controleur;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 524);
		setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//comboBox.setEditable(true);
		comboBox = new JComboBox();
		comboBox.setActionCommand("choixC");
		comboBox.addActionListener(controleur);
		comboBox.setBounds(26, -2, 318, 24);
		contentPane.add(comboBox);
		
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setActionCommand("ajouter");
		btnAjouter.addActionListener(controleur);
		btnAjouter.setBounds(26, 189, 114, 25);
		contentPane.add(btnAjouter);
		
		JButton btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setActionCommand("Supprimer");
		btnSupprimer.addActionListener(controleur);
		btnSupprimer.setBounds(152, 189, 114, 25);
		contentPane.add(btnSupprimer);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setActionCommand("quit");
		btnQuitter.addActionListener(controleur);
		btnQuitter.setBounds(26, 226, 114, 25);
		contentPane.add(btnQuitter);
		
		lblAffichageClub = new JLabel();
		lblAffichageClub.setBounds(26, 34, 746, 24);
		contentPane.add(lblAffichageClub);
		
		AffichageListLicencie = new JTextArea();
		AffichageListLicencie.setBounds(26, 53, 447, 108);
		contentPane.add(AffichageListLicencie);
		
		JPanel zoneAjout = new JPanel();
		zoneAjout.setBorder(new EmptyBorder(5, 5, 5, 5));
		zoneAjout.setBounds(0, -29, 784, 411);
		contentPane.add(zoneAjout);
		zoneAjout.setLayout(null);
		
		JTextArea nomArea = new JTextArea();
		nomArea.setBounds(37, 287, 186, 25);
		zoneAjout.add(nomArea);
		
		JTextArea prenomArea = new JTextArea();
		prenomArea.setBounds(37, 331, 186, 25);
		zoneAjout.add(prenomArea);
		
		JTextArea dateNArea = new JTextArea();
		dateNArea.setBounds(37, 368, 186, 25);
		zoneAjout.add(dateNArea);
		
		JButton button_2 = new JButton("Enregistrer");
		button_2.setBounds(65, 407, 114, 25);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		zoneAjout.add(button_2);
		button_2.setActionCommand("enregister");
	
	}
	
	public void remplirCombo(List<Club> list) {
		
		//Club c2 = new Club("001","Le Mans FC","Ren� Sance","Paul E444mploi ");
		//list.add(c2);
		
		//comboBox.removeAllItems();
		//comboBox.addItem(new Club()); //marche passsssssssssssssss
		comboBox.addItem("-");
		for (Club c : list) {
			System.out.println(c.getNom());
			comboBox.addItem(c.getNom());
			//comboBox.setVisible(false);
			
		}
	
	}

	public void majPanel(char c) {
		// a faire
		
	}

	public void affichageInfoClub(String cc) {
		
		
		
		lblAffichageClub.setText(cc);
		
	}


	public void affichageLicencie(List<Licencie> recupAllByClub) {
		
		AffichageListLicencie.setText("");
		for (Licencie l : recupAllByClub) {
			
			AffichageListLicencie.setText(AffichageListLicencie.getText() + "\n" + l.getNom() + " " +l.getPrenom());
			
		}
		
	}
}
