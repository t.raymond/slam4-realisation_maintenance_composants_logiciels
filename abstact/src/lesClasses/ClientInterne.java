package lesClasses;

public class ClientInterne extends Salarie{
	
	private double soldeCompte;
	
	
	
	
	

	public ClientInterne(String nom, String prenom, int salaire, double soldeCompte) {
		super(nom, prenom,salaire);
		this.soldeCompte = soldeCompte;
	}



	public double getSoldeCompte() {
		return soldeCompte;
	}



	public void setSoldeCompte(double soldeCompte) {
		this.soldeCompte = soldeCompte;
	}



	@Override
	public void verserSalaire() {
		// TODO Auto-generated method stub
		soldeCompte += getSalaire();
		
		System.out.println("solde du compte mis à jour : " + soldeCompte + "€");
	}



	@Override
	public String toString() {
		return super.toString() + "ClientInterne [soldeCompte=" + soldeCompte + "€ ]";
	}
	
	
	
	

}
