package lesClasses;

public abstract class Salarie {
	
	private String nom;
	private String prenom;


	private int salaire;
	
	
	
	

	public Salarie(String nom, String prenom, int salaire) {
		this.nom = nom;
		this.prenom = prenom;
		this.salaire = salaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public int getSalaire() {
		return salaire;
	}
	
	public abstract void verserSalaire();
	
	
	public String toString() {
		return "Salarie [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "€ ]";
	}

}
