package geometrie;

public class TriangleRectangle extends Triangle{

	public TriangleRectangle(double cote1, double cote2, double cote3) {
		super(cote1, cote2, cote3);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		double longueur = 0, largeur = 0;
		if ((cote1 > cote2)&& (cote1 > cote3)) {
			 longueur = cote2;
			largeur = cote3;
		}
		else if ((cote2 > cote1)&& (cote2 > cote3)) {
			longueur = cote1;
			largeur = cote3;
		}
		else if ((cote3 > cote2)&& (cote3 > cote1)) {
			longueur = cote2;
			largeur = cote1;
		}
		return (longueur*largeur)/2;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n Triangle rectangle :" + "\nLes 3 cotes mesurent : "+cote1+"cm et "+cote2+"cm et "+cote3+"cm" + "\n- Périmètre = " + perimetre() + " - Surface = " + surface();
	}

}
