package geometrie;

public class Rectangle extends Quadrilatere {

	public Rectangle(double cote1, double cote2) {
		super(cote1, cote2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double perimetre() {
		// TODO Auto-generated method stub
		return (cote1 + cote2)*2;
	}

	@Override
	public String toString() {
		return super.toString() + "\n rectangle :" + "\nLes 2 cotes mesurent : "+cote1+"cm et "+cote2+"cm" + "\n- Périmètre = " + perimetre() + " - Surface = " + surface();
	}
	

}
