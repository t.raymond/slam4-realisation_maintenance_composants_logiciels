package geometrie;

public abstract class Quadrilatere extends FigureGeo {

	protected double cote1, cote2;
	
	 public Quadrilatere(double cote1, double cote2)
	 {
	     this.cote1 = cote1;
	     this.cote2 = cote2;
	 }

	@Override
	public abstract double perimetre();

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return cote1*cote2;
	}

	@Override
	public String toString() {
		return "R est une forme géométrique de type :";
	}


}

