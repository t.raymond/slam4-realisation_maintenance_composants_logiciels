$(function(){
   /*********************************** Page connexion************************************/  
                        
    $('#btnconnexion').click( function(e) {
                        // les deux lignes annulent le comportement par défaut du clic
                        // c'est à dire href="#" qui provoquerait un rappel de la même page
                          e.preventDefault();
                          e.stopPropagation();
                          var mdp = $("#mdp").val(); //récupère le contenue de la zone d'id mdp
                          var login = $("#login").val();
                          /* appel d'une fonction ajax -> post*/
                          // elle prend 3 arguments :
                          // 1- le nom de la fonction php qui sera exécutée
                          // 2- la liste des arguments à fournir à cette fonction
                          // 3- le nom de la fonction JS qui sera exécutée au "retour" du serveur 
                          $.post("ajax/traiterconnexion.php",{
                              // valorise les deux arguments passés à la fonction traiterconnexion.php
                                "mdp" : mdp,        
                                "login" : login},
                                foncRetourConnexion );
       });

     /* fonction JS qui sera exécutée après le retour de l'appel ajax précedent */
     // le paramètre data représente la donnée envoyée par le serveur
     // résultat de l'appel de la fonction retourConnexion.php
    function foncRetourConnexion(data){
            if(data.length != 0){
            // charge la page (data-role=page) du même document dont l'id  est le sélecteur indiqué
                $.mobile.changePage("#pagemenuaccueil");
             }
             else{
             // sinon affichage d'un message dans la div d'id message
                $("#message").css({'color':'red'});
                $("#message").html("erreur de login et/ou mdp");
             }
    }
    
    
    
    
    /***************************************** Page inscription*******************************/
                    
    $('#btninscription').click( function(e) { 
                                e.preventDefault();
                                e.stopPropagation();
                                var nom = $("#nom").val();
                                var prenom = $("#prenom").val();
                                var mail = $("#mail").val();
                                var tel = $("#tel").val();
                                $.post("ajax/enregistreruser.php",{
                                    "nom" : nom,
                                    "prenom" : prenom,
                                     "mail" : mail,
                                     "tel"  : tel,
                                     "type" :  $("input[type=radio][name=type]:checked").attr("value")},
                                    foncRetourEnregistrement );     
                                
    });

    function foncRetourEnregistrement(data){
                 $("#divinscrit").html(data);
    }
                   

    /*******************************    ******************************/    
    
    
    $("#lstoffres > li").click( function() {
    var id = $(this).attr("id");
    $.post("ajax/traiteroffre.php",{"ido" : id}, retourOffre,"json" );
    
    
    });
 
    function retourOffre(data)
    {
        $("#nom").html(data["nom"]);
        $("#prenom").html(data["prenom"]);
        $("#tel").attr("href", "tel:"+ data["tel"]);
        $("#mail").attr("href","mailto: " + data["mail"]);
        if (data["ramassage"] !== undefined){

          $("#lieux").children("li").remove();

          for (var i = 0; i < data["ramassage"].length; i++){

            $("#lieux").append("<li>"+data["ramassage"][i]["lieu"]+"</li>");
          }
        //console.log(data["ramassage"] === undefined);//if true!!!!
        //console.log(data["ramassage"][0]["lieu"]);
  

        }
    }


    $('#btnsuppr').click( function(e) {
                          e.preventDefault();
                          e.stopPropagation();
                          var lesid = [];
                          $("input[type='checkbox']:checked").each(
                            function() {var i = $(this).attr('id');
                            $("label[for='"+i+"']").remove();
                            $(this).remove();
                            lesid.push(i);
                          });
                          //console.log(lesid.toString());
                          $.post("ajax/traitersuppression.php",{"lesid[]" : lesid});
       });
    
    
}); // fin fonction principale/* 


