<div data-role="page" id="pagemenuaccueil">
<?php
include "vues/back.html";
include "vues/logo.html";
?>
 <div data-role="content" id="divinscription">    
    <div id="divmenuaccueil">
        <ul data-role="listview" >
            <li><a href="index.php?action=gereroffresarriveeentreprise" rel="external">Voir toutes les offres "Arrivée" entreprise</a></li>
          <!-- l'attribut rel="external" permet de faire appel à un lien externe sans passer par ajax -->  
            <li><a href="index.php?action=gereroffresdepartentreprise" rel="external">Voir toutes les offres "Départ" entreprise</a></li>
            <li><a href="index.php?action=gerermesoffres" rel="external">Gérer mes offres</a></li>
        </ul> 
    </div>
 </div>  <!-- /content -->   
    
<?php
 include "vues/pied.html";
?>
</div><!-- /page -->
