<?php

// code non encore écrit qui retournera le login s'il est présent en base ou une chaine vide,
// à faire dans une itération suivante
function verifuser($login, $mdp)
{
    $tab = GetLesUsers();
    $verif = "";
    foreach($tab as $user){
        
        if($login == $user["login"]&& $mdp == $user["mdp"]){
            
         $verif = $user["id"];
            
        }
    }
    
    
    
  return $verif;
}

function GetLesUsers(){
$tab=array(
array("id"=>"c1","login"=>"pdurand","mdp"=>"abcd12"),
array("id"=>"c2","login"=>"amarquis","mdp"=>"efg543"),
array("id"=>"c3","login"=>"mlabourot","mdp"=>"xyz987"),
array("id"=>"c1","login"=>"a","mdp"=>"a"));
return $tab;
}

// retourne un tableau trié sur les jours croissants de la semaine
function getLesOffresDepartEntreprise()
{
  $tab = array(
           array("id" => 1,
                 "idchauffeur" => "c1",
                 "jour" => "mardi",
                 "date" => "permanent",
                 "heure" => "15h",
                 "retour" => "paris 20",
                 "nom" => "Durand",
                 "prenom" => "Pierre",
                 "mail" => "pierre.durand@sio.com",
                 "tel" => "0148592370"),
           array("id" => 2,
                 "idchauffeur" => "c2",
                 "jour" => "mardi",
                 "date" => "03/12/2019",
                 "heure" => "16h",
                 "retour" => "Métro Robespierre",
                 "nom" => "Marquis",
                 "prenom" => "Aurore",
                 "mail" => "aurore.marquis@sio.com",
                 "tel" => "0688300169"),
           array("id" => 3,
                 "idchauffeur" => "c3",
                 "jour" => "mercredi",
                 "date" => "04/12/2019",
                 "heure" => "17h30",
                 "retour" => "Bobigny Mairie",
                 "nom" => "Labourot",
                 "prenom" => "Manon",
                 "mail" => "manon.labourot@sio.com",
                 "tel" => "01458798"),
           array("id" => 4,
                 "idchauffeur" => "c1",
                 "jour" => "mercredi",
                 "date" => "permanent",
                 "heure" => "17h",
                 "retour" => "paris 20",
                 "nom" => "Durand",
                 "prenom" => "Pierre",
                 "mail" => "pierre.durand@sio.com",
                 "tel" => "0148592370"),
           array("id" => 5,
                 "idchauffeur" => "c2",
                 "jour" => "vendredi",
                 "date" => "29/11/2019",
                 "heure" => "17h30",
                 "retour" => "Métro Robespierre",
                 "nom" => "Marquis",
                 "prenom" => "Aurore",
                 "mail" => "aurore.marquis@sio.com",
                 "tel" => "0688300169"),
           array("id" => 6,
                 "idchauffeur" => "c3",
                 "jour" => "vendredi",
                 "date" => "permanent",
                 "heure" => "15h30",
                 "retour" => "Bobigny Mairie",
                 "nom" => "Labourot",
                 "prenom" => "Manon",
                 "mail" => "manon.labourot@sio.com",
                 "tel" => "01458798"),
         );
  return $tab;
}

function getLesOffresArriveeEntreprise()
{
  $tab = array(
           array("id" => 7, 
                "idchauffeur" => "c1", 
                "jour" => "lundi",
                "date" => "permanent", 
                "heure" => "8h",
                "ramassage" => array(
                                  array("id" => 1, 
                                        "lieu" => "porte des lilas"),
                                  array("id" => 2, 
                                        "lieu" => "porte de bagnolet")),
                "depart" => "paris 20", 
                "nom" => "Durand", 
                "prenom" => "Pierre",
                "mail" => "pierre.durand@societe.com", 
                "tel"=>"0148592370"),
           array("id" => 8, 
                "idchauffeur" => "c1", 
                "jour" => "mardi",
                "date" => "17/12/2019", 
                "heure" => "9h",
                "ramassage" => array(
                                  array("id" => 1, 
                                        "lieu" => "porte des lilas"),
                                  array("id" => 2, 
                                        "lieu" => "porte de bagnolet")),
                "depart" => "paris 20", 
                "nom" => "Durand", 
                "prenom" => "Pierre",
                "mail" => "pierre.durand@societe.com", 
                "tel" => "0148592370"),
            array("id" => 9, 
                  "idchauffeur" => "c2", 
                  "jour" => "mardi",
                  "date" => "17/12/2019", 
                  "heure" => "7h30",
                  "ramassage" => array(
                                    array("id" => 1, 
                                          "lieu" => "Mairie de Montreuil"),
                                    array("id" => 2, 
                                          "lieu" => "A3 Villiers")),
                  "depart" => "Métro Robespierre", 
                  "nom" => "Marquis",
                  "prenom" => "Aurore",
                  "mail" => "aurore.marquis@societe.com",
                  "tel" => "0688300169"),
            array("id" => 10, 
                  "idchauffeur" => "c2", 
                  "jour" => "mercredi",
                  "date" => "permanent", 
                  "heure" => "8h",
                  "ramassage" => array(
                                    array("id" => 1, 
                                          "lieu" => "Mairie de Montreuil"),
                                    array("id" => 2, 
                                          "lieu" => "A3 Villiers")),
                  "depart" => "Métro Robespierre", 
                  "nom" => "Marquis",
                  "prenom" => "Aurore", 
                  "mail" => "aurore.marquis@societe.com", 
                  "tel" => "0688300169"),
            array("id" => 11, 
                  "idchauffeur" => "c1", 
                  "jour" => "vendredi",
                  "date" => "20/12/2019", 
                  "heure" => "7h30",
                  "ramassage" => array(
                                    array("id" => 1, 
                                          "lieu" => "porte des lilas"),
                                    array("id" => 2, 
                                          "lieu" => "porte de bagnolet")),
                  "depart" => "Paris 20", 
                  "nom" => "Durand", 
                  "prenom" => "Pierre",
                  "mail" => "pierre.durand@societe.com", 
                  "tel" => "0148592370"),
            array("id" => 12, 
                  "idchauffeur" => "c3", 
                  "jour" => "vendredi",
                  "date" => "permanent", 
                  "heure" => "8h",
                  "ramassage" => array(
                                    array("id" => 1, 
                                          "lieu" => "Bobigny A3"),
                                    array("id" => 2, 
                                          "lieu" => "Bondy")),
                  "depart" => "Bobigny Mairie", 
                  "nom" => "Labourot", 
                  "prenom" => "Manon",
                  "mail" => "manon.labourot@societe.com", 
                  "tel" => "0145879823"),
                            );
    return $tab;
}

function getOffreDById($id)
{

  $lesoffres = getLesOffresDepartEntreprise();
  $uneoffre = array();

  foreach($lesoffres as $o)
  {
    if($o["id"] == $id)
    {
      $uneoffre = $o;
    }
  }

  return $uneoffre;
}

function getOffreAById($id)
{

  $lesoffres = getLesOffresArriveeEntreprise(); 
  $uneoffre = array();

  foreach($lesoffres as $o)
  {
    if($o["id"] == $id)
    {
      $uneoffre = $o;
    }
  }

  return $uneoffre;
}

function getmesOffresById($id,$type)
{

    if ($type == "A"){
    $lesoffres = getLesOffresArriveeEntreprise();}
    else if ($type == "D"){
    $lesoffres = getLesOffresDepartEntreprise();}
    
    $uneoffre = array();

  foreach($lesoffres as $o)
  {
    if($o["idchauffeur"] == $id)
    {
      array_push($uneoffre,$o);
    }
  }

  return $uneoffre;
}

?>
