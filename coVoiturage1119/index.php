<?php
session_start();
require_once 'util/fonctions.php';
include "vues/entete.html";


if(!isset($_REQUEST['action']))
    $action = 'accueil';
else 
    $action = $_REQUEST['action'];

switch($action)
{
    case 'accueil':
        include "vues/pageconnexion.php";
        include "vues/pagemenuaccueil.php";
        break;
    case 'inscription':
        include "vues/pageinscription.php";
        break;
    case 'gereroffresdepartentreprise':
        $lesOffres = getLesOffresDepartEntreprise();
	$_SESSION["offre"]="D";
        include "vues/pageoffresoffertes.php";
        include "vues/pageoffre.php";
        break;
    case 'gereroffresarriveeentreprise':
        $lesOffres = getLesOffresArriveeEntreprise();
	$_SESSION["offre"]="A";
        include "vues/pageoffresoffertes.php";
        include "vues/pageoffre.php";
        break;
    case 'gerermesoffres':
        $mesOffresA = getmesOffresById($_SESSION["connexion"],"A");
	$mesOffresD = getmesOffresById($_SESSION["connexion"],"D");
        include "vues/pagegerermesoffres.php";
        break;
 }





?>
</body>
</html>
