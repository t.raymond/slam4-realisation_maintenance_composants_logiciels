package test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import slam.GestionAjoutUsers;
import slam.User;

public class GestionAjoutUsersTest extends TestCase {
/*
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		junit.textui.TestRunner.run(GestionAjoutUsersTest.class);

	}*/
	private GestionAjoutUsers gau;
	
	@Before
	protected void setUp() throws Exception {
		try {
			this.gau = new GestionAjoutUsers("testusers.xml");
		} catch (IOException e) {
			fail("Création de l'OUT impossible !");
		}
	}

	@Test
	public void test2PremiersCarsGenereId() {
		
	String id = this.gau.genereId("Casimir", "Martinmatin");
	assertTrue("La taille de l'id est trop petite", id.startsWith("cmartinmat"));
	
	String id2 = this.gau.genereId("Abû Ja`Far Muhammad", "Mūsā Khwārezmī");
	int tailleid2 = id2.length();
	assertEquals("la taille de l'id est trop grande", tailleid2,10);
		
	String id3 = this.gau.genereId("Abû Ja`Far Muhammad", "Mūsā Khwārezmī");
	assertTrue("les espaces ont été pris en compte", id3.startsWith("amūsākhwār"));
	//System.out.println(id3);
	
	}
	
	@Test
	public void testajoutUser() {
	
	User id4 = this.gau.ajoutUser("Paul", "Leduc");
	User id5 = this.gau.ajoutUser("Pierre", "Leduc");
	assertNotSame("le même id",id4,id5);
	
	}
	
	@Test
	public void test1gmdp() {
	
	String mdp1 = this.gau.genereMotPass(6);
	int taillemdp1 = mdp1.length();
	assertEquals("la taille du mdp est différente de 8", taillemdp1,8);
	
	}
	
	@Test
	public void test2gmdp() {
	
	
	String mdp1 = this.gau.genereMotPass(6);
	String mdp2 = this.gau.genereMotPass(6);
	assertNotEquals("le mot de passe est identique", mdp1,mdp2);
	
	}

}