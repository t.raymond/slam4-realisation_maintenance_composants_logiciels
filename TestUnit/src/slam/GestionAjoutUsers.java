package slam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import util.XPathUtil;

public class GestionAjoutUsers {
  
	// Logger  : cet objet permettra d'émettre un message dans la console par exemple
  private static final Log logger = LogFactory.getLog(GestionAjoutUsers.class);

   // le nom logique du fichier des users
   private String fileName;

  // une collection de type dictionnaire : cle=>id de type String, val=>une instance de User
  private Map<String, User> lesUsers;

  /**
   * 
   * @param fileName
   *          le fichier des users E/S
   * @throws Exception
   *           en cas de pb E/S lors de la lecture (loadFromFile) du fichier
   */
  public GestionAjoutUsers(String fileName) throws IOException {
    this.fileName = fileName;
    this.lesUsers = new HashMap<String, User>();
    loadFromFile();
  }

  /**
   * Place le contenu du fichier XML des users dans une collection de type dictionnaire en mémoire.
   * 
   * @throws IOException  : des exceptions sont susceptibles d'être levées ici mais non gérées
   */
  public void loadFromFile() throws IOException {

    lesUsers.clear();

    File f = new File(fileName);
    if (!f.exists()) {  // si le fichier n'existe pas, il faut le créer avec un minimum de contenu (balise parent <users>)
      f.createNewFile();
      saveToFile();
    }

    URL url = f.toURI().toURL();

    String expressionXPath = "users/user";

    NodeList nodes = XPathUtil.eval(url.openStream(), expressionXPath);

    // itération sur les balise enfants (noeuds <user>) 
    
    for (int e = 0; e < nodes.getLength(); e++) {
      Node node = nodes.item(e);
      NodeList enfants = node.getChildNodes();
      String nom = "";
      String prenom = "";
      String id = "";
      String mdp = "";
      // pour récupérer les noeuds "données" des tags (items) <nom>,<prenom>,<id> et <mdp>
      for (int i = 0; i < enfants.getLength(); i++) {
        String balise = enfants.item(i).getLocalName();
        String contenu = enfants.item(i).getTextContent().trim();
        if ("prenom".equals(balise))
          prenom = contenu;
        else if ("nom".equals(balise))
          nom = contenu;
        else if ("id".equals(balise))
          id = contenu;
        else if ("mdp".equals(balise))
          mdp = contenu;
      }
      lesUsers.put(id, new User(prenom, nom, id, mdp));  // on insert alors un nouvel élément User dans le dictionnaire
    }
  }

  /**
   * Enregistre les users sur disque (dans le fichier xml)
   * 
   * @throws IOException
   */
  public void saveToFile() throws IOException {
    FileOutputStream fo = new FileOutputStream(fileName);
    PrintWriter pw = new PrintWriter(fo);
    // toString redéfini ici pour générer du XML
    pw.print(this.toString());
    pw.close();
  }

  /**
   * Ajoute un utilisateur à la liste en cours (dans le dictionnaire)
   * 
   * @param prenom
   *          prénom de l'utilisateur à ajouter
   * @param nom
   *          nom de l'utilisateur à ajouter
   * @return 
   * 		  l'instance User ainsi créée
   */
  public User ajoutUser(String prenom, String nom) {
    String id = genereId(prenom, nom);
    String mdp = genereMotPass(8);
    int index = 1;
    while(lesUsers.containsKey(id)){
    	id = genereId(prenom.substring(index), nom);
    	index++;
    }
    User user = new User(prenom, nom, id, mdp);
    lesUsers.put(id, user);
    logger.trace(user);
    return user;
  }

  /**
   * La méthode toString a été complètement redéfinie ici. 
   * A partir des éléments du dictionnaire "users", elle "construit" le code xml 
   * correspondant.
   */
  @Override
  public String toString() {
    StringBuffer res = new StringBuffer();
    res.append("<?xml version='1.0' ?>\n");
    res.append("<users>\n");
    
    // pour parcourir le dictionnaire, on définit un "Iterator" 
    Iterator<User> users = lesUsers.values().iterator();  

    while (users.hasNext()) {
      res.append(users.next());
      res.append("\n");
    }

    res.append("</users>");
    return res.toString();
  }

  /**
   * Génération d'un id unique (par rapport à  la liste)
   * 
   * @param prenom
   *          prénom
   * @param nom
   *          nom
   * @return un id de type chaine de caractères
   */
  public String genereId(String prenom, String nom) {	  
	String id = ((prenom.substring(0,1) + nom).toLowerCase()).replaceAll(" ", "");
    
	if (id.length() > 10) {
		id=id.substring(0,10);
	}
	else if (id.length() < 5) {
		int taille = 5 - id.length();
		Random rd = new Random();
	for (int i = 0; i<taille;i++)
		{
			id+=""+rd.nextInt(10); 
		}
	
	}
	return id;
  }

  /**
   * Génération d'un mot de passe
   * 
   * @param taille
   *          longueur souhaitée pour le mdp
   * @return un mot de passe de type chaîne de caractères
   */
  public String genereMotPass(int taille) {
	  String mdp="";
	  if (taille != 8) {
		  taille = 8;
	  }
	  Random rd = new Random();
	  boolean ok = true;
    int j = 0;

    while(ok) {

      mdp="";
	  for (int i = 1; i<=taille;i++)
	  {
		  mdp+=""+rd.nextInt(10); 
	  }

    j = 0;
	  
	  for (User u : lesUsers.values()){
		  if (u.getMdp().equals(mdp)) {
			  j++;
		  }}

    if (j==0){
    ok = false}
	  }



    
	  return mdp;
}
}
