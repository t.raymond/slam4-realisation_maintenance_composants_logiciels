package org.example.edf_raymond;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {
	
	private String requete="create table Client ("
			+ "idC INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ "nom TEXT NOT NULL,"
			+ "prenom TEXT NOT NULL,"
			+ "adresse TEXT NOT NULL,"
			+ "codePostal TEXT NOT NULL,"
			+ "ville TEXT NOT NULL,"
			+ "telephone TEXT NOT NULL,"
			+ "idCompteur TEXT NOT NULL,"
//			+ "ancienReleve DOUBLE NOT NULL,"
//			+ "dateAncienReleve TEXT NOT NULL,"
			+ "releve DOUBLE NOT NULL,"
			+ "dateReleve TEXT NOT NULL,"
			+ "signatureBase64 TEXT NOT NULL,"
			+ "situation INTEGER NOT NULL);";
	

	public BdSQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(requete);
		
		db.execSQL("insert into Client (nom,prenom,adresse,codePostal,ville,telephone,idCompteur,/*ancienReleve,dateAncienReleve,*/releve,dateReleve,signatureBase64,situation) values('jack','prenom','10 Rue du Bouquet','72100','Le Mans','0612345678','C1',/*0.1,'12.01.2015',*/0.4,'12.01.2015','AE',1);");
		
		ContentValues value = new ContentValues();
		value.put("nom","François");
		value.put("prenom","Bertrand");
		value.put("adresse","1 Place de L Hôtel de ville");
		value.put("codePostal","72310");
		value.put("ville","Bessé-sur-Braye");
		value.put("telephone","0654958569");
		value.put("idCompteur","C2");
		//value.put("ancienReleve",0.0);
		//value.put("dateAncienReleve","20.10.2018");
		value.put("releve",0.1);
		value.put("dateReleve","25.11.2018");
		value.put("signatureBase64","AC5");
		value.put("situation",1);
		db.insert("Client", null, value);
		value.put("nom","Reckt");
		value.put("prenom","Habib");
		value.put("adresse","10 Rue des Remparts");
		value.put("codePostal","24100");
		value.put("ville","Bergerac");
		value.put("telephone","0654958577");
		value.put("idCompteur","C3");
		//value.put("ancienReleve",0.5);
		//value.put("dateAncienReleve","20.10.2019");
		value.put("releve",0.1);
		value.put("dateReleve","25.11.2020");
		value.put("signatureBase64","AE5");
		value.put("situation",0);
		db.insert("Client", null, value);
		value.put("nom","Guau");
		value.put("prenom","Flavie");
		value.put("adresse","10 Rue Girardon");
		value.put("codePostal","52000");
		value.put("ville","Chaumont");
		value.put("telephone","0654428579");
		value.put("idCompteur","C4");
		//value.put("ancienReleve",22.0);
		//value.put("dateAncienReleve","01.01.2019");
		value.put("releve",22.1);
		value.put("dateReleve","03.12.2020");
		value.put("signatureBase64","AED");
		value.put("situation",1);
		db.insert("Client", null, value);
		value.put("nom","Tyty");
		value.put("prenom","Didier");
		value.put("adresse","9 Rue de l'Église");
		value.put("codePostal","80200");
		value.put("ville","Assevillers");
		value.put("telephone","0652386512");
		value.put("idCompteur","C5");
		//value.put("ancienReleve",30.0);
		//value.put("dateAncienReleve","01.01.2019");
		value.put("releve",35.1);
		value.put("dateReleve","01.12.2019");
		value.put("signatureBase64","ED3");
		value.put("situation",1);
		db.insert("Client", null, value);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
