package org.example.edf_raymond;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    int num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton ib1 = findViewById(R.id.ib1);
        ib1.setOnClickListener(this);
        ImageButton ib2 = findViewById(R.id.ib2);
        ib2.setOnClickListener(this);
        ImageButton ib3 = findViewById(R.id.ib3);
        ib3.setOnClickListener(this);
        ImageButton ib4 = findViewById(R.id.ib4);
        ib4.setOnClickListener(this);
        //testBD();
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.ib1:
                Toast.makeText(this, "Bouton cliqué n1: ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ib2:
                Intent intentJeu = new Intent(this, AfficheListeClientActivity.class);
                this.startActivityForResult(intentJeu,num);
                break;
            case R.id.ib3:
                Toast.makeText(this, "Bouton cliqué n3: ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ib4:
                Toast.makeText(this, "Bouton cliqué n4: ", Toast.LENGTH_SHORT).show();
                break;
        }
    }
    public void testBD(){
        Log.i("TEST","TestBD...<============================================================================================");
        ClientDAO ClientAcces = new ClientDAO(this);
        Log.i("TEST","arrivée dans TestBD....");

        //Récupération list client
        ArrayList<Client> lesClients = ClientAcces.getClients("%");

        for (Client unClient : lesClients) {
            Log.i("TEST","recup list client...= "+ unClient.toString());
        }


        //Ajout de deux client
        Client leClient = new Client(1,"testnom","testPrenom","TestAdresse","00000","testVille","0600000000","AAA",1.1,"00.00.0000");
        Log.i("TEST","ajout client 1...= "+ Long.toString(ClientAcces.addClient(leClient)));
        Client leClient2 = new Client(2,"testnom","testPrenom2","TestAdresse","00000","testVille","0600000000","AAA",1.1,"00.00.0000");
        Log.i("TEST","ajout client 2...= "+ Long.toString(ClientAcces.addClient(leClient2)));

        //ClientAcces.addClient(leClient);

        //Récupération d'un client
        Client leClientRecup = ClientAcces.getClient(1);
        Log.i("TEST","récupération client N1...= "+ leClientRecup.toString());

        //Update Client
        leClientRecup.setPrenom("COUCOU");
        Log.i("TEST","Edit client 1...= "+ Long.toString(ClientAcces.EditClient(leClientRecup)));

        //Delete
        Client ClientN3 = ClientAcces.getClient(3);
        Log.i("TEST","delete client N3...= "+ Long.toString(ClientAcces.deleteClient(ClientN3)));

        //Récupération list clients
        lesClients = ClientAcces.getClients("%");

        for (Client unClient : lesClients) {
            Log.i("TEST","recup list client...= "+ unClient.toString());
        }
        Log.i("TEST","TestBD...<============================================================================================");

    }
}
