package org.example.edf_raymond;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeolocalisationActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private LocationManager locationManager;
    private String fournisseur, adresse;
    private LatLng positionClient, positionAgent;
    private Boolean geolocAgent, geolocClient;
    private SupportMapFragment MapFragment;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    ClientDAO ClientAcces = new ClientDAO(this);

    public void recupPositionAgent() {// récupère la position géolocalisée, et met à jour la propriété geolocAgent
        geolocAgent = false;
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteres = new Criteria();
        //précision fine ACCURACY_FINE et moin bonne = ACCURACY_COARSE
        criteres.setAccuracy(Criteria.ACCURACY_FINE);
        //l'altitude
        criteres.setAltitudeRequired(true);
        //direction
        criteres.setBearingRequired(true);
        //vitesse
        criteres.setSpeedRequired(true);
        //consomation énergie
        criteres.setCostAllowed(true);
        criteres.setPowerRequirement(Criteria.POWER_HIGH);

        fournisseur = locationManager.getBestProvider(criteres, true);
        //si pas de fournisseur
        if (fournisseur == null || fournisseur.equals("")) {
            Toast.makeText(this, "GéoLoc immpossible", Toast.LENGTH_SHORT).show();
        } else {
            //provider = fournisseur de position
            //minTime = la periode entre deux maj en ms
            //minDistance = la période entre deux maj en m
            //listener = l'écouteur qui sera lancé dès que le fournisseur sera activé

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(fournisseur, 20000, 0, this);
            Location loca = locationManager.getLastKnownLocation(fournisseur);
            if(loca != null){
                positionAgent = new LatLng(loca.getLatitude(),loca.getLongitude());
                geolocAgent = true;
            }else {
                positionAgent = new LatLng(47.958423, 0.166806);
                geolocAgent = true;
                Toast.makeText(this, "GéoLoc immpossible 2", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void recupPositionClient(){// détermine une position à partir d'une adresse et met à jour la propriété geolocClient
        geolocClient = false;
        Geocoder fwdGeocoder = new Geocoder(this, Locale.FRANCE);
        List<Address> locations = null;
        try{
            locations = fwdGeocoder.getFromLocationName(adresse,10);
        } catch (IOException e){
            Toast.makeText(this, "pb géocoder adresse", Toast.LENGTH_SHORT).show();
        }
        if((locations == null) || (locations.isEmpty())){
            Toast.makeText(this, "adresse inconnu", Toast.LENGTH_SHORT).show();
        }else {
            positionClient = new LatLng(locations.get(0).getLatitude(),locations.get(0).getLongitude());
            geolocClient = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocalisation);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        Intent mIntent = getIntent();
        long id = mIntent.getLongExtra("id",-1);
        if(id >= 0){
            Client leClient = ClientAcces.getClient(id);
            adresse = leClient.getAdresse() + "," + leClient.getCodePostal() + " " + leClient.getVille() + "France";
            recupPositionAgent();
            recupPositionClient();
        }
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(geolocClient && geolocAgent) {
            googleMap.addMarker(new MarkerOptions()
                    .position(positionClient)
                    .title("Client")
                    .snippet("Point de rdv prochain client"));
            builder.include(positionClient);
            googleMap.addMarker(new MarkerOptions()
                    .position(positionAgent)
                    .title("Agent")
                    .snippet("position géolocalisée"));
            builder.include(positionAgent);

            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),this.getResources().getDisplayMetrics().widthPixels,
                    this.getResources().getDisplayMetrics().heightPixels,100));
        }
        else {
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
