package org.example.edf_raymond;

import java.util.Calendar;

public class Client {

    private long identifiant;
    private String nom,prenom,adresse,codePostal,ville,telephone;
    private String idCompteur;
    private Double ancienReleve;
    private String dateAncienReleve;
    // Données qui seront à saisir lors d’une visite
    private Double releve;
    private String dateReleve;
    // pour simplifier la date sera une chaîne
    private String signatureBase64;
    private int situation;
    /* Exemples de situation client -----
    * *  0 client non traité par défaut
    * *  1 Absent
    * *  2 Absent mais relevé possible sans signature client
    * *  3 Présent, relevé ok mais pas de signature car pas représentant légal
    * *  4 Présent et tout ok
    * *  5 Déménagé / logement vide
    * *  6 Déménagé / nouveaux locataires * */

    public Client() {
        this.identifiant = identifiant;
        this.nom = "";
        this.prenom = "";
        this.adresse = "";
        this.codePostal = "";
        this.ville = "";
        this.telephone = "";
        this.idCompteur = "";
        this.ancienReleve = 0.0;
        this.dateAncienReleve = "";
        this.releve = 0.0;
        this.dateReleve = "";
        this.signatureBase64 = "";
        this.situation = 0;
    }

    public Client(long identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String idCompteur, Double ancienReleve, String dateAncienReleve) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.idCompteur = idCompteur;
        this.ancienReleve = ancienReleve;
        this.dateAncienReleve = dateAncienReleve;
        this.releve = 0.0;
        this.dateReleve = Calendar.getInstance().getTime().toString();
        this.signatureBase64 = "";
        this.situation = 0;
    }

    public long getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(long identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIdCompteur() {
        return idCompteur;
    }

    public void setIdCompteur(String idCompteur) {
        this.idCompteur = idCompteur;
    }

    public Double getAncienReleve() {
        return ancienReleve;
    }

    public void setAncienReleve(Double ancienReleve) {
        this.ancienReleve = ancienReleve;
    }

    public String getDateAncienReleve() {
        return dateAncienReleve;
    }

    public void setDateAncienReleve(String dateAncienReleve) {
        this.dateAncienReleve = dateAncienReleve;
    }

    public Double getReleve() {
        return releve;
    }

    public void setReleve(Double releve) {
        this.releve = releve;
    }

    public String getDateReleve() {
        return dateReleve;
    }

    public void setDateReleve(String dateReleve) {
        this.dateReleve = dateReleve;
    }

    public String getSignatureBase64() {
        return signatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        this.signatureBase64 = signatureBase64;
    }

    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }

    public String toString() {
        return "Client{" +
                "identifiant=" + identifiant +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", telephone='" + telephone + '\'' +
                ", idCompteur='" + idCompteur + '\'' +
                ", ancienReleve=" + ancienReleve +
                ", dateAncienReleve='" + dateAncienReleve + '\'' +
                ", releve=" + releve +
                ", dateReleve='" + dateReleve + '\'' +
                ", signatureBase64='" + signatureBase64 + '\'' +
                ", situation=" + situation +
                '}';
    }
}
