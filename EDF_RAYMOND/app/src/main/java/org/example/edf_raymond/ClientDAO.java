package org.example.edf_raymond;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class ClientDAO {

	
	private static String base = "BDClient";
	private static int version = 1;
	private BdSQLiteOpenHelper accesBD;
	
	public ClientDAO(Context ct){
		accesBD = new BdSQLiteOpenHelper(ct, base, null, version);
		
	}

	public long addClient(Client unClient){
		long ret;
		SQLiteDatabase bd = accesBD.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("nom",unClient.getNom());
		value.put("prenom",unClient.getPrenom());
		value.put("adresse",unClient.getAdresse());
		value.put("codePostal",unClient.getCodePostal());
		value.put("ville",unClient.getVille());
		value.put("telephone",unClient.getTelephone());
		value.put("idCompteur",unClient.getIdCompteur());
		//value.put("ancienReleve",unClient.getAncienReleve());
		//value.put("dateAncienReleve",unClient.getDateAncienReleve());
		value.put("releve",unClient.getReleve());
		value.put("dateReleve",unClient.getDateReleve());
		value.put("signatureBase64",unClient.getSignatureBase64());
		value.put("situation",unClient.getSituation());
		ret = bd.insert("Client", null, value);
		
		return ret;
	}

	public long EditClient(Client unClient){
		long ret;
		SQLiteDatabase bd = accesBD.getWritableDatabase();

		ContentValues value = new ContentValues();
		value.put("nom",unClient.getNom());
		value.put("prenom",unClient.getPrenom());
		value.put("adresse",unClient.getAdresse());
		value.put("codePostal",unClient.getCodePostal());
		value.put("ville",unClient.getVille());
		value.put("telephone",unClient.getTelephone());
		value.put("idCompteur",unClient.getIdCompteur());
		//value.put("ancienReleve",unClient.getAncienReleve());
		//value.put("dateAncienReleve",unClient.getDateAncienReleve());
		value.put("releve",unClient.getReleve());
		value.put("dateReleve",unClient.getDateReleve());
		value.put("signatureBase64",unClient.getSignatureBase64());
		value.put("situation",unClient.getSituation());
		ret = bd.update("Client", value,"idC = ?", new String[] { Long.toString(unClient.getIdentifiant()) });

		return ret;
	}
	
		
	public Client getClient(long idC){
		Client leClient = null;
		Cursor curseur;
		curseur = accesBD.getReadableDatabase().rawQuery("select * from Client where idC="+idC+";",null);
		if (curseur.getCount() > 0) {
			curseur.moveToFirst();
			leClient = new Client(idC,curseur.getString(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),curseur.getDouble(8),curseur.getString(9));
			leClient.setSituation(curseur.getInt(11));
		}
		return leClient;
	}

	public long deleteClient(Client unClient){
		long ret;
		SQLiteDatabase bd = accesBD.getWritableDatabase();

		ret = bd.delete("Client","idC = ?", new String[] { Long.toString(unClient.getIdentifiant()) });

		return ret;
	}
	
	public ArrayList<Client> getClients(String nom){
		Cursor curseur;
		String req = "select * from Client where nom like '"+nom+"';";
		curseur = accesBD.getReadableDatabase().rawQuery(req,null);
		return cursorToClientArrayList(curseur);
	}
	
	
	private ArrayList<Client> cursorToClientArrayList(Cursor curseur){
		ArrayList<Client> listeClient = new ArrayList<Client>();
		long idC;
		String nom;
		String prenom;
		String adresse;
		String codePostal;
		String ville;
		String telephone;
		String idCompteur;
		Double releve;
		String dateReleve;
		
		curseur.moveToFirst();
		while (!curseur.isAfterLast()){
			idC = curseur.getLong(0);
			nom = curseur.getString(1);
			prenom = curseur.getString(2);
			adresse = curseur.getString(3);
			codePostal = curseur.getString(4);
			ville = curseur.getString(5);
			telephone = curseur.getString(6);
			idCompteur = curseur.getString(7);
			releve = curseur.getDouble(8);
			dateReleve = curseur.getString(9);
			listeClient.add(new Client(idC,nom,prenom,adresse,codePostal,ville,telephone,idCompteur,releve,dateReleve));
			curseur.moveToNext();
		}
		
		return listeClient;
	}
	
	
}
