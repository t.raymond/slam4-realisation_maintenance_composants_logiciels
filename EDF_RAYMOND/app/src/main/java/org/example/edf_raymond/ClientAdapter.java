package org.example.edf_raymond;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ClientAdapter extends BaseAdapter {

    public List<Client> listClient;
    public LayoutInflater layoutInflater;

    private class ViewHolder
    {
        TextView textViewId;
        TextView textViewNom;
        TextView textViewPrenom;
        TextView textViewTelephone;
        TextView textViewAdresse;
        TextView textViewCodePostal;
        TextView textViewVille;
    }

    public ClientAdapter(Context context, List<Client> listClient) {
        this.layoutInflater = LayoutInflater.from(context);
        this.listClient = listClient;
    }
    @Override
    public int getCount() {
        return listClient.size();
    }

    @Override
    public Object getItem(int position) {
        return listClient.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listClient.get(position).getIdentifiant();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if(convertView == null){
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.vue_client, null);
            holder.textViewId = (TextView) convertView.findViewById(R.id.vueIdentifiant);
            holder.textViewNom = (TextView) convertView.findViewById(R.id.vueNom);
            holder.textViewPrenom = (TextView) convertView.findViewById(R.id.vuePrenom);
            holder.textViewVille = (TextView) convertView.findViewById(R.id.vueVille);
            holder.textViewAdresse = (TextView) convertView.findViewById(R.id.vueAdresse);
            holder.textViewCodePostal = (TextView) convertView.findViewById(R.id.vueCp);
            holder.textViewTelephone = (TextView) convertView.findViewById(R.id.vueTI);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textViewId.setText(""+listClient.get(position).getIdentifiant());
        holder.textViewNom.setText(listClient.get(position).getNom());
        holder.textViewPrenom.setText(listClient.get(position).getPrenom());
        String tel = listClient.get(position).getTelephone();
        tel = String.format("%s.%S.%s.%s.%s", tel.substring(0,2), tel.substring(2,4), tel.substring(4,6), tel.substring(6,8),tel.substring(8,10));
        holder.textViewTelephone.setText(tel);
        holder.textViewVille.setText(listClient.get(position).getVille());
        holder.textViewCodePostal.setText(listClient.get(position).getCodePostal());
        holder.textViewAdresse.setText(listClient.get(position).getAdresse());
        if(position % 2 == 0){
            convertView.setBackgroundColor(Color.rgb(238,233,233));
        }
        else{
            convertView.setBackgroundColor(Color.rgb(255,255,255));
        }
        return convertView;
    }
}
