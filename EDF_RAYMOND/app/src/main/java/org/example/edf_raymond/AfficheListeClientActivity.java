package org.example.edf_raymond;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class AfficheListeClientActivity extends Activity {
    int num;
    ClientDAO ClientAcces = new ClientDAO(this);
    ListView listView;
    List<Client> listeClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichelisteclient);
        listeClient = ClientAcces.getClients("%");

        listView = (ListView) findViewById(R.id.lvListe);

        ClientAdapter clientAdapter = new ClientAdapter(this, listeClient);
        listView.setAdapter(clientAdapter);
        listView.setOnItemClickListener(myOnItemClickListener);
    }

    AdapterView.OnItemClickListener myOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intentModif = new Intent(AfficheListeClientActivity.this, ModificationClientActivity.class);
            intentModif.putExtra("id",id);
            startActivity(intentModif);
        }

    };
}
