package org.example.edf_raymond;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ModificationClientActivity extends Activity implements View.OnClickListener{
    ClientDAO ClientAcces = new ClientDAO(this);
    Client leClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificationclient);
        Intent mIntent = getIntent();
        long id = mIntent.getLongExtra("id",-1);
        if(id >= 0){
            leClient = ClientAcces.getClient(id);
            EditText identifiant = (EditText) findViewById(R.id.id);
            EditText identite = (EditText) findViewById(R.id.ide);
            EditText tel = (EditText) findViewById(R.id.tel);
            EditText adresse = (EditText) findViewById(R.id.adresse);
            EditText compteur = (EditText) findViewById(R.id.compteur);
            EditText ancienReleve = (EditText) findViewById(R.id.ar);
            EditText ancienDate = (EditText) findViewById(R.id.ad);
            EditText releveCompteur = (EditText) findViewById(R.id.rc);
            EditText date = (EditText) findViewById(R.id.date);
            EditText situation = (EditText) findViewById(R.id.s);

            identifiant.setText(String.valueOf(leClient.getIdentifiant()));
            String identiteText = leClient.getNom() + " " + leClient.getPrenom();
            identite.setText(identiteText);
            tel.setText(leClient.getTelephone());
            adresse.setText(leClient.getAdresse());
            compteur.setText(leClient.getIdCompteur());
            ancienReleve.setText(String.valueOf(leClient.getAncienReleve()));
            ancienDate.setText(leClient.getDateAncienReleve());
            releveCompteur.setText(String.valueOf(leClient.getReleve()));
            date.setText(leClient.getDateReleve());
            situation.setText(String.valueOf(leClient.getSituation()));



        }else {
            finish();
        }
        Button btnGEO = findViewById(R.id.geo);
        btnGEO.setOnClickListener(this);
        Button btnENG = findViewById(R.id.eng);
        btnENG.setOnClickListener(this);
        Button btnAN = findViewById(R.id.annuler);
        btnAN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.geo:
                Intent intentGeo = new Intent(ModificationClientActivity.this, GeolocalisationActivity.class);
                intentGeo.putExtra("id",leClient.getIdentifiant());
                startActivity(intentGeo);
                break;
            case R.id.eng:
                save();
                break;
            case R.id.annuler:
                finish();
                break;
        }
    }
    public void save(){
        int ret;
        EditText releveCompteur = (EditText) findViewById(R.id.rc);
        EditText date = (EditText) findViewById(R.id.date);
        EditText situation = (EditText) findViewById(R.id.s);
        int Vsituation = Integer.parseInt(situation.getText().toString());
        double Vreleve = Double.parseDouble(releveCompteur.getText().toString());
        if(Vsituation <= 0 || Vsituation > 6){
            AlertDialog alertDialog = new AlertDialog.Builder(ModificationClientActivity.this).create();
            alertDialog.setTitle("Alerte !");
            alertDialog.setMessage("Situation doit être égal 1,2,3,4,5 ou 6");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

        } else if(Vreleve <= leClient.getAncienReleve() && Vsituation > 1 && Vsituation < 5){
            AlertDialog alertDialog = new AlertDialog.Builder(ModificationClientActivity.this).create();
            alertDialog.setTitle("Alerte !");
            alertDialog.setMessage("Relevé compteur > ancien relevé, sauf avec situation = 1 ou 5 ou 6");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else {
            leClient.setReleve(Vreleve);
            leClient.setDateReleve(date.getText().toString());
            leClient.setSituation(Vsituation);
            ClientAcces.EditClient(leClient);
            finish();
            Toast.makeText(this, "Enregistrer", Toast.LENGTH_SHORT).show();
        }
    }

}
