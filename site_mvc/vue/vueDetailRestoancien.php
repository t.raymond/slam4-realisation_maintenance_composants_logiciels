
<h1><?= $unResto['nom']; ?></h1>

<span id="note"></span>
<section>
    Cuisine <br />
</section>
<p id="principal">   
    <?= $unResto['desc_resto']; ?>
</p>
<h2 id="adresse">
    Adresse
</h2>
<p>
    <?= $unResto['num_adr']; ?>
    <?= $unResto['voie_adr']; ?><br />
    <?= $unResto['cp']; ?>
    <?= $unResto['ville']; ?>
</p>

<h2 id="photos">
    Photos
</h2>
<ul id="galerie">
</ul>

<h2 id="horaires">
    Horaires
</h2> 
<?= $unResto['horaires']; ?>

<h2 id="crit">Critiques</h2>
<ul id="critiques">
</ul>

